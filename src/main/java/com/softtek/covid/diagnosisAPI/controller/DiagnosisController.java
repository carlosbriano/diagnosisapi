package com.softtek.covid.diagnosisAPI.controller;

import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;

import com.softtek.covid.diagnosisAPI.model.DiagnosisRequest;
import com.softtek.covid.diagnosisAPI.model.DiagnosisResponse;
import com.softtek.covid.diagnosisAPI.model.GoogleResponse;
import com.softtek.covid.diagnosisAPI.model.Translation;
import com.softtek.covid.diagnosisAPI.service.DiagnosisService;
import com.softtek.covid.diagnosisAPI.service.TranslateService;


@RestController
public class DiagnosisController {
	
	  @PostMapping("/diagnosis")
	  public ResponseEntity postDiagnosis(@Valid @RequestBody DiagnosisRequest diagnosis)
	      throws ResourceAccessException {
		  DiagnosisResponse diagnosisRes=DiagnosisService.callDiagnosisApi(diagnosis);
		  DiagnosisResponse diagnosisTrans=TranslateService.translateDiagnosis(diagnosisRes);
		  return ResponseEntity.ok().body(diagnosisTrans);
	  }
	  @GetMapping("/diagnosis")
	  public ResponseEntity getDiagnosis()
	      throws ResourceAccessException {
		  return null;
	  }
}
