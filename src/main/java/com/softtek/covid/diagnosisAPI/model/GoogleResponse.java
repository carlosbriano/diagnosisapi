package com.softtek.covid.diagnosisAPI.model;

public class GoogleResponse {
	
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
	
}
