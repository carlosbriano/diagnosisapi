package com.softtek.covid.diagnosisAPI.model;

public class Translation {
	public String translatedText;

	public String getTranslatedText() {
		return translatedText;
	}

	public void setTranslatedText(String translatedText) {
		this.translatedText = translatedText;
	}
	
	
}
