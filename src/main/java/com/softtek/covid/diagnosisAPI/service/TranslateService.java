package com.softtek.covid.diagnosisAPI.service;
import com.softtek.covid.diagnosisAPI.model.DiagnosisResponse;
import com.softtek.covid.diagnosisAPI.model.GoogleResponse;
import com.softtek.covid.diagnosisAPI.model.Item;
import com.softtek.covid.diagnosisAPI.utils.ApplicationEndpoint;
import com.softtek.covid.diagnosisAPI.utils.TranslatePathConstructor;
import org.springframework.web.client.RestTemplate;

public class TranslateService {
	
	public static DiagnosisResponse translateDiagnosis (DiagnosisResponse diagnosisBody) {	
		TranslatePathConstructor googleApi= new TranslatePathConstructor("en", "es", diagnosisBody.getQuestion().getText(), "AIzaSyCmlds0m9CXCf6BWfK3_0LcHCfaoZP7xVk", "text");
		RestTemplate restTemplate=new RestTemplate();
		GoogleResponse translatedDiagnosis= restTemplate.postForObject(googleApi.getPath() , null, GoogleResponse.class );
		googleApi.setText(diagnosisBody.getQuestion().getText());
		translatedDiagnosis= restTemplate.postForObject(googleApi.getPath() , null, GoogleResponse.class );
		diagnosisBody.getQuestion().setText(translatedDiagnosis.getData().getTranslations()[0].translatedText);
		Item[] itemsArray=diagnosisBody.getQuestion().getItems();
		for(int i=0;i<itemsArray.length;i++) {
			googleApi.setText(itemsArray[i].getExplanation());
			translatedDiagnosis= restTemplate.postForObject(googleApi.getPath() , null, GoogleResponse.class );
			diagnosisBody.getQuestion().getItems()[i].setExplanation(translatedDiagnosis.getData().getTranslations()[0].translatedText);
			googleApi.setText(itemsArray[i].getName());
			translatedDiagnosis= restTemplate.postForObject(googleApi.getPath() , null, GoogleResponse.class );
			diagnosisBody.getQuestion().getItems()[i].setName(translatedDiagnosis.getData().getTranslations()[0].translatedText);
		}
		return diagnosisBody;
	}
	
}
