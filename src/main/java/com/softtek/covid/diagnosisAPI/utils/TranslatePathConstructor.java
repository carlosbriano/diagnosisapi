package com.softtek.covid.diagnosisAPI.utils;

public class TranslatePathConstructor {
	private String sourceLanguage;
	private String targetLanguage;
	private String text;
	private String apiKey;
	private String format;
	private static final String basePath=ApplicationEndpoint.TRANSLATOR;
	
	public TranslatePathConstructor(String sourceLanguage, String targetLanguage, String text, String apiKey, String format) {
		this.sourceLanguage = sourceLanguage;
		this.targetLanguage = targetLanguage;
		this.text = text;
		this.apiKey = apiKey;
		this.format = format;
	}
	
	public String getPath() {
		return basePath.concat("?"+"q="+text+"&"+"target="+targetLanguage+"&"+"format="+format+"&"+"source="+sourceLanguage+"&"+
				"key="+apiKey);
	}

	public String getSourceLanguage() {
		return sourceLanguage;
	}

	public void setSourceLanguage(String sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}

	public String getTargetLanguage() {
		return targetLanguage;
	}

	public void setTargetLanguage(String targetLanguage) {
		this.targetLanguage = targetLanguage;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
	
}

